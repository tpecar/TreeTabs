# Please note

**This fork is currently not intended for further development of TreeTabs.**

It is intended *only* for containing a:
- wishlist / roadmap of features that I would like to see implemented
- guide for setting up the development environment
- configuration files for Gitlab CI/CD set-up

with the intention of merging these additions back upstream.
